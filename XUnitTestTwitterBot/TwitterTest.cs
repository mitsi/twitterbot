using System;
using System.Collections.Generic;
using System.IO;
using Tweetinvi.Models;
using TwitterBot;
using Xunit;

namespace XUnitTestTwitterBot
{
    public class TwitterTest
    {
        [Fact]
        public void AnimalsPatternArraysAreFilled()
        {
            Twitter twitterConnection = CreateTwitterConnection();
            Assert.True(twitterConnection.MessagePatternDog.Count == 12);
            Assert.True(twitterConnection.MessagePatternCat.Count == 12);
            Assert.True(twitterConnection.MessagePatternDuck.Count == 15);
        }

        [Fact]
        public void CanDetectBannedWord()
        {
            Twitter twitterConnection = CreateTwitterConnection();
            Assert.False(twitterConnection.NoBannedWords("Come on Guys vote for the best !"));
            Assert.False(twitterConnection.NoBannedWords("A trap for contest bot !"));
            Assert.True(twitterConnection.NoBannedWords("RT + Follow pour gagner"));
        }


        [Fact]
        public void CanDownloadCatImage()
        {
            string hashtag = new CatPicture().Download("imageTestCat.jpg");
            Assert.Equal("#cat", hashtag);
            Assert.True(File.Exists("imageTestCat.jpg"));
        }

        [Fact]
        public void CanDownloadDuckImage()
        {
            string hashtag = new DuckPicture().Download("imageTestDuck.jpg");
            Assert.Equal("#duck", hashtag);
            Assert.True(File.Exists("imageTestDuck.jpg"));
        }

        [Fact]
        public void CanDownloadDogImage()
        {
            string hashtag = new DogPicture().Download("imageTestDog.jpg");
            Assert.Equal("#dog", hashtag);
            Assert.True(File.Exists("imageTestDog.jpg"));
        }

        [Fact]
        public void IsPatternFollowWellMatched()
        {
            Twitter twitter = CreateTwitterConnection();
            Assert.True(twitter.HasKeyWordToFollow("follow"));
            Assert.True(twitter.HasKeyWordToFollow("#follow"));
            Assert.True(twitter.HasKeyWordToFollow("Follow"));
            Assert.True(twitter.HasKeyWordToFollow("#Follow"));
            Assert.True(twitter.HasKeyWordToFollow("FOLLOW"));
            Assert.True(twitter.HasKeyWordToFollow("#FOLLOW"));
            Assert.True(twitter.HasKeyWordToFollow("following"));
            Assert.True(twitter.HasKeyWordToFollow("#following"));
            Assert.True(twitter.HasKeyWordToFollow("FOLLOWING"));
            Assert.True(twitter.HasKeyWordToFollow("#FOLLOWING"));
            Assert.True(twitter.HasKeyWordToFollow("Following"));
            Assert.True(twitter.HasKeyWordToFollow("#Following"));
        }

        private Twitter CreateTwitterConnection()
        {
            return new Twitter(
                Environment.GetEnvironmentVariable("TWITTER_CONSUMER_KEY"),
                Environment.GetEnvironmentVariable("TWITTER_CONSUMER_SECRET"),
                Environment.GetEnvironmentVariable("TWITTER_ACCESS_TOKEN"),
                Environment.GetEnvironmentVariable("TWITTER_ACCESS_TOKEN_SECRET"));
        }
    }
}

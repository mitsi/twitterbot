FROM microsoft/dotnet:2.1-sdk
WORKDIR /source
COPY . .
RUN dotnet restore
ENTRYPOINT ["dotnet", "test"]

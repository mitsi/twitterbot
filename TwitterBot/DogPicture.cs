﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace TwitterBot
{
    public class DogPicture
    {
        public string Download(string filename)
        {           
            string rawJson;

            using (var web = new WebClient())
            {
                var url = "https://dog.ceo/api/breeds/image/random";
                rawJson = web.DownloadString(url);
            }

            if (rawJson == "")
            {
                Console.WriteLine("Error downloading json");
                return "";
            }

            dynamic json = JsonConvert.DeserializeObject(rawJson);

            using (var web = new WebClient())
            {
                web.DownloadFile(json.message.Value.ToString(), filename);
            }   
            
            return "#dog";
        }
    }
}
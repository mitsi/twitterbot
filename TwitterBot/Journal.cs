﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterBot
{
    public enum Journal : long
    {
        FIGARO = 8350912,
        GORAFI = 492648852,
        MONDE = 24744541,
        HUFFPOST = 364513528,
        COMMITSTRIP = 2414205444
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace TwitterBot
{
    public class Twitter
    {
        private static Random random = new Random();
        private static readonly string[] BannedUser = { "bot", "spot", "lvbroadcasting", "jflessauSpam", "bryster125", "MobileTekReview", "ilove70315673", "followandrt2win", "traunte", "ericsonabby", "_aekkaphon" };
        private static readonly string[] Bannedwords = { "vote", "bot", "b0t" };

        public static readonly string[] GamePatternFr = { "Follow + RT", "RT + Follow", "RT & Follow", "#RT #Follow" };
        public static readonly string[] GamePatternEn = { "rt to", "rt and win", "retweet and win", "rt for", "rt 4", "retweet to" };
        public static readonly string[] MessagePatternAnimal =
        {
            "Wonderful", "Wow such beautiness", "Nature is Amazing !", "Yay !", "*_*", "OwO",
            "Mais regarde sa bouille !!", "Sauvage", "Chasseur", "Bonjour", "Fluffy !!"
        };       
        public static string[] MessagePatternRetweet =
        {
            "Il est pour moi !", "Je le veux :D", "Je tente ma chance !", "La force est avec moi B-)", "Ho !!",
            "NAN MAIS VAS Y JE VEUX GAGNER !!", "Pow pow pow :D", "Et c'est parti :)", "C'est pour moi !", ";)",
            "Et un de plus :3"
        };

        public List<string> MessagePatternCat;
        public List<string> MessagePatternDog;
        public List<string> MessagePatternDuck;

        public Twitter(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
        {
            MessagePatternCat = new List<string>() {"Cat life"};
            MessagePatternDog = new List<string>() {"Une vie de chien :p !"};
            MessagePatternDuck = new List<string>() {"Coin Coin!", "Duck Ducky", "Coin :3", "Canarddddddddddd"};
            Auth.SetUserCredentials(consumerKey, consumerSecret, accessToken, accessTokenSecret);            
            foreach (string s in MessagePatternAnimal)
            {
                MessagePatternCat.Add(s);
                MessagePatternDog.Add(s);                
                MessagePatternDuck.Add(s);                
            }
        }

        public void RetweetGameWithPattern(string pattern, int nb=1)
        {                    
            foreach (var newTweet in GetNewTweets(SearchTweets(pattern, LanguageFilter.French), nb, GetTweetsFromTimeLine()))
            {
                PlayGame(newTweet);
            }
        }

        public List<ITweet> GetTweetsFromTimeLine(int nbOfTweet = 400)
        {
            return TimelineAsync.GetUserTimeline(User.GetAuthenticatedUser().Id, nbOfTweet).GetAwaiter().GetResult().ToList();
        }

        public List<ITweet> GetAllTweetsFromTimeline()
        {
            RateLimit.RateLimitTrackerMode = RateLimitTrackerMode.TrackAndAwait;

            RateLimit.QueryAwaitingForRateLimit += (sender, args) =>
            {
                Console.WriteLine($"Query : {args.Query} is awaiting for rate limits!");
            };
            
            var lastTweets = Timeline.GetUserTimeline(User.GetAuthenticatedUser().Id, 200).ToArray();

            var allTweets = new List<ITweet>(lastTweets);
            var beforeLast = allTweets;

            while (lastTweets.Length > 0 && allTweets.Count <= 3200)
            {
                var idOfOldestTweet = lastTweets.Select(x => x.Id).Min();
                Console.WriteLine($"Oldest Tweet Id = {idOfOldestTweet}");

                var numberOfTweetsToRetrieve = allTweets.Count > 3000 ? 3200 - allTweets.Count : 200;
                var timelineRequestParameters = new UserTimelineParameters
                {
                    MaxId = idOfOldestTweet - 1,
                    MaximumNumberOfTweetsToRetrieve = numberOfTweetsToRetrieve
                };

                lastTweets = Timeline.GetUserTimeline(User.GetAuthenticatedUser().Id, timelineRequestParameters).ToArray();
                allTweets.AddRange(lastTweets);
            }
            return allTweets;
        }

        public List<ITweet> SearchTweets(string subject, LanguageFilter languageFilter)
        {
            var searchParameter = new SearchTweetsParameters(subject)
            {
                Lang = languageFilter,
                SearchType = SearchResultType.Popular,
                MaximumNumberOfResults = 50,
                Since = DateTime.Today.AddDays(-3)
        };
            return Search.SearchTweets(searchParameter).ToList();
        }

        public List<ITweet> GetNewTweets(List<ITweet> searchTweets, int nb, List<ITweet> tweetsFromTimeLine)
        {
            int i = 0;
            List<ITweet> newTweets = new List<ITweet>();
            foreach (ITweet tweet in searchTweets)
            {
                if (i >= nb)
                {
                    return newTweets;
                }

                if (IsTweetValid(tweet, tweetsFromTimeLine))
                {
                    newTweets.Add(tweet);
                    i++;
                }
            }

            return newTweets;
        }

        public bool IsTweetValid(ITweet tweet, List<ITweet> userTimeline)
        {
            if (!IsUserAuthorized(tweet.CreatedBy.Name))
            {
                Console.WriteLine("The user tweet is banned");
                return false;
            }

            if (!NoBannedWords(tweet.FullText))
            {
                Console.WriteLine("The tweet contains a banned word");
                return false;
            }

            if (tweet.Retweeted)
            {
                Console.WriteLine("The tweet is a repost");
                return false;
            }

            if (userTimeline.FirstOrDefault(t => t.RetweetedTweet != null && t.RetweetedTweet.Id == tweet.Id) != null)
            {
                Console.WriteLine("I have already retweeted it");
                return false;
            }

            return true;
        }

        public bool NoBannedWords(string tweetFullText)
        {
            foreach (var bw in Bannedwords)
            {
                if (tweetFullText.Contains(bw))
                {
                    Console.WriteLine($"{bw} in {tweetFullText}");
                    return false;
                }
            }

            return true;
        }

        public void PlayGame(ITweet tweet)
        {
            Retweet(tweet);

            FollowIfNeeded(tweet);
        }

        public void Retweet(ITweet tweet)
        {
            Tweet.PublishRetweet(tweet);
        }

        private void FollowIfNeeded(ITweet tweet)
        {
            if (!HasKeyWordToFollow(tweet.FullText))
            {
                return;
            }

            IEnumerable<long> ids = User.GetAuthenticatedUser().GetFollowerIds() as long[] ??
                                    User.GetAuthenticatedUser().GetFollowerIds().ToArray();
            if (!ids.Contains(tweet.CreatedBy.Id))
            {
                Console.WriteLine($"I follow the creator : {tweet.CreatedBy.Name}");
                User.FollowUser(tweet.CreatedBy.Id);
            }

            tweet.UserMentions.ForEach(user =>
            {
                if (user.Id != null)
                {
                    if (!ids.Contains(user.Id.Value))
                    {
                        Console.WriteLine($"I follow the mentioned user : {user.Name}");
                        User.FollowUser(user.Id.Value);
                    }
                }
            });
        }

        public void SendRandomPicture(PictureType pictureType)
        {
            string filename = "img.jpg";
            string message;
            string hashtag;
            switch (pictureType)
            {
                case PictureType.CAT:
                    hashtag = new CatPicture().Download(filename);
                    message = $"{MessagePatternCat[random.Next(0, MessagePatternCat.Count)]} {hashtag}";
                    break;
                case PictureType.DOG:
                    hashtag = new DogPicture().Download(filename);
                    message = $"{MessagePatternDog[random.Next(0, MessagePatternDog.Count)]} {hashtag}";
                    break;
                default:
                    hashtag = new DuckPicture().Download(filename);
                    message = $"{MessagePatternDuck[random.Next(0, MessagePatternDuck.Count)]} {hashtag}";
                    break;
            }

            var image = File.ReadAllBytes(filename);
            var media = Upload.UploadBinary(image);
            Tweet.PublishTweet($"{message}", new PublishTweetOptionalParameters
            {
                Medias = { media }
            });
        }

        public ITweet GetLastTweetFrom(long userId)
        {
            return TimelineAsync.GetUserTimeline(userId, 1).GetAwaiter().GetResult().ToList()[0];
        }

        public bool HasKeyWordToFollow(string content)
        {
            return content.ToLower().Contains("follow");
        }

        private bool IsUserAuthorized(string username)
        {
            string sanitizedUsername = username.Replace("0", "o").ToLower();
            return BannedUser.FirstOrDefault(s => s == sanitizedUsername) == null;
        }

        public long GetIdFromUsername(String userName)
        {
            IUser user = User.GetUserFromScreenName(userName);
            return user.Id;
        }
    }
}
